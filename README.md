Speedstream 5200 password reset utility
=======================================

This software is an alternative of
[Gabriel Strong's FTP reset utility][] designed to run on non-Windows
platforms.

It requires a Linux environment or, on FreeBSD, access to the Berkeley
Packet Filter, to generate raw ethernet frames.

Usage instructions
------------------

Configuration is compiled into code, because I was too lazy to parse
commandline arguments.

So you will first need to edit the `pwreset.c` file to change the
following configuration:

 * source MAC address - your machine
 * target MAC address - the device you want to reset

On FreeBSD:

 * interface to communicate on

On Linux:

 * IP addres of the router to send the packet to the right interface

By default the login/password is admin/admin, but you can also change
that.

Next, compile the program and run it as root:

    make
    sudo ./pwreset

If the packet was properly sent, you should see the device respond
with a similar packet (see below for the exact details). Use `tcpdump`
to make sure it does, or you can just go blindly and assume it works.

The next step is to download the `x.cfg` file from the router,
example:

    ncftpget ftp://admin:admin@192.168.254.254/x.cfg

You can then modify that file to change various parameters, see
[this forum post][] for examples.

 [this forum post]: http://forums.redflagdeals.com/unlocking-speedstream-5200-act-full-featured-router-307731/

Then you can push the modified config file:

    ncftpput -c ftp://admin:admin@192.168.254.254 / x.cfg

If you're lucky enough, the `admin`/`admin` password combination
should also work for telnet, see [the telnet forum post][] for more
information on that.

 [the telnet forum post]: https://secure.dslreports.com/forum/r21660224-

Legal notes and prior art
-------------------------

This program was inspired from [Gabriel Strong's FTP reset utility][],
specifically, I used the [documented packet structure][] to figure out
which packets to generate.

 [Gabriel Strong's FTP reset utility]: http://www.gabrielstrong.com/
 [documented packet structure]: http://www.gabrielstrong.com/ethernet_packet.php

It is, however a "clean room" implementation as I haven't had the
chance to look at Gabriel's source code, which doesn't seem to be
public.

I assume this is legal as it provides valuable service for owners
of those devices. Furthermore, it seems to follow a documented
standard and in no way abuses flaws or vulnerabilities in the
device. I did not personnally reverse engineer anything and relied
on public documentation above to write this software.

The said protocol is [SNAP][]

 [SNAP]: https://en.wikipedia.org/wiki/Subnetwork_Access_Protocol

Technical details
-----------------

For the record, we should be sending the following payload:

    0000  00 0b 23 9d 83 1a 00 00  24 cc 93 44 00 6e aa aa   ..#..... $..D.n..
    0010  03 00 20 ea 02 02 00 00  01 03 00 60 00 00 00 01   .. ..... ...`....
    0020  00 00 24 cc 93 44 c0 a8  00 0d ff ff ff 00 00 0b   ..$..D.. ........
    0030  23 9d 83 1a c0 a8 00 fe  ff ff ff 00 61 64 6d 69   #....... ....admi
    0040  6e 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00   n....... ........
    0050  00 00 00 00 00 00 00 00  00 00 00 00 61 64 6d 69   ........ ....admi
    0060  6e 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00   n....... ........
    0070  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00   ........ ........
    0080  00 00 00 00 00 00 00 00  00 00                     ........ ..

It will be parsed by tcpdump as such:

    02:22:46.558825 00:00:24:cc:93:44 > 00:0b:23:9d:83:1a SNAP Unnumbered, ui, Flags [Command], length 124

On success, the device should answer something like:

    02:22:46.559002 00:0b:23:9d:83:1a > 00:00:24:cc:93:44 SNAP Unnumbered, ui, Flags [Command], length 46
            0x0000:  aaaa 0300 20ea 0202 0000 0105 0008 0000  ................
            0x0010:  0001 0000 0001 0000 0000 0000 0000 0000  ................
            0x0020:  0000 0000 0000 0000 0000 0000 0000       ..............

