/*
 * Copyright © 2013 The Anarcat <anarcat@orangeseeds.org>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#ifdef __FreeBSD__
#include <net/bpf.h>
#endif

/* should be prompted, but i couldn't be bothered with getopt... */

/* those two are stripped at 14 chars */
char *login = "admin";
char *password = "admin";
/*
 * used to route the packet to the right interface by the kernel make
 * sure you choose an IP that will make the packet go out on the right
 * interface
 */
#ifdef __FreeBSD__
/* necessary to send the data on the right BPF interface */
const char* interface = "vr0";
#else
/* used by linux to find the MAC of the target */
char *target_ip = "192.168.2.1";
#endif
/* those are still necessary outside of MAC headers */
uint8_t sender_mac[6] = { 0x00, 0x00, 0x24, 0xcc, 0x93, 0x44}; /* 00:00:24:cc:93:44 */
uint8_t target_mac[6] = { 0x00, 0x0b, 0x23, 0x9d, 0x83, 0x1a}; /* 00:0b:23:9d:83:1a */

/* no serviceable parts below */

typedef struct _arp_pkt arp_pkt;
struct _arp_pkt {
#ifdef __FreeBSD__
  uint8_t target_mac[6];
  uint8_t sender_mac[6];
  uint16_t plen; /* 0x006E */
#endif
  uint16_t unknown1; /* 0xAAAA */
  uint8_t control; /* 0x03 */
  uint8_t vendor[3]; /* 0x0020EA */
  uint16_t htype; /* 0x0202 */
  uint8_t unknown2[10]; /* 0x00000103006000000001 */
  uint8_t sender_mac2[6];
  uint8_t unknown3[8]; /* C0A8000DFFFFFF00 */
  uint8_t target_mac2[6];
  uint8_t unknown4[8]; /* C0A800FEFFFFFF00 */
  uint8_t login[14];
  uint8_t unknown5[18]; /* 0000... */
  uint8_t password[14];
  uint8_t unknown6[32]; /* 000... */
};

const uint8_t vendor[3] = { 0x00, 0x20, 0xEA };
const uint8_t unknown2[10] = { 0x00, 0x00, 0x01, 0x03, 0x00, 0x60, 0x00, 0x00, 0x00, 0x01};
const uint8_t unknown3[8] = { 0xC0, 0xA8, 0x00, 0x0D, 0xFF, 0xFF, 0xFF, 0x00 };
const uint8_t unknown4[8] = { 0xC0, 0xA8, 0x00, 0xFE, 0xFF, 0xFF, 0xFF, 0x00 };

#ifdef __FreeBSD__
/* those two functions taken from http://www.vankuik.nl/2012-02-09_Writing_ethernet_packets_on_OS_X_and_BSD */
// Try to open the bpf device
int open_dev(void)
{
	char buf[ 11 ] = { 0 };
        int bpf = 0;
        int i = 0;
        for(i = 0; i < 99; i++ )
        {
            sprintf( buf, "/dev/bpf%i", i );
            bpf = open( buf, O_RDWR );
            if( bpf != -1 ) {
                printf("Opened device /dev/bpf%i\n", i);
                break;
            }
        }
        if(bpf == -1) {
            printf("Cannot open any /dev/bpf* device, exiting\n");
            exit(1);
        }
        return bpf;
}

// Associate bpf device with a physical ethernet interface
void assoc_dev(int bpf, char* interface)
{
	struct ifreq bound_if;
	strcpy(bound_if.ifr_name, interface);
        if(ioctl( bpf, BIOCSETIF, &bound_if ) > 0) {
            printf("Cannot bind bpf device to physical device %s, exiting\n", interface);
            exit(1);
        }
        printf("Bound bpf device to physical device %s\n", interface);
}

void set_raw(int bpf)
{
	if(ioctl(bpf, BIOCSHDRCMPLT, 1) > 0) {
	    perror("cannot set raw mode on bpf device");
	}
        printf("set raw mode on bpf device\n");
}
#endif

int main(int argc, char **argv) {
  int sd, i, bytes;
  arp_pkt pkt;
  uint8_t *ptr;

#ifdef __FreeBSD__
  sd = open_dev();
  assoc_dev(sd, (char*) interface);
  set_raw(sd);
#else
  struct sockaddr_in sin;
  if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
    perror ("socket() failed to get socket descriptor for using ioctl() ");
    exit (EXIT_FAILURE);
  }
  printf("Setup RAW socket\n");
#endif

  // Copy source MAC address.
#ifdef __FreeBSD__
  memcpy (pkt.sender_mac, sender_mac, 6 * sizeof (uint8_t));
#endif
  /* sender_mac MAC filled in by Linux */
  memcpy (pkt.sender_mac2, sender_mac, 6 * sizeof (uint8_t));

  // Report source MAC address to stdout.
  printf ("Source MAC address is ");
  for (i=0; i<5; i++) {
    printf ("%02x:", sender_mac[i]);
  }
  printf ("%02x\n", sender_mac[5]);

#ifdef __FreeBSD__
  memcpy (pkt.target_mac, target_mac, 6 * sizeof (uint8_t));
  pkt.plen = 0x6E00; /* backwards? */
#endif
  memcpy (pkt.target_mac2, target_mac, 6 * sizeof (uint8_t));

  // Report target MAC address to stdout.
  printf ("Target MAC address is ");
  for (i=0; i<5; i++) {
    printf ("%02x:", target_mac[i]);
  }
  printf ("%02x\n", target_mac[5]);

  /* clear memory of those just to be safe */
  for (i=0; i<sizeof(pkt.login); i++) {
    pkt.login[i] = 0x00;
  }
  for (i=0; i<sizeof(pkt.password); i++) {
    pkt.password[i] = 0x00;
  }
  memcpy (pkt.login, login, ( strlen(login) > sizeof(pkt.login) ? sizeof(pkt.login) : strlen(login)));
  memcpy (pkt.password, password, ( strlen(password) > sizeof(pkt.password) ? sizeof(pkt.password) : strlen(password)));

  memcpy (pkt.vendor, vendor, sizeof(pkt.vendor));
  memcpy (pkt.unknown2, unknown2, sizeof(pkt.unknown2));
  memcpy (pkt.unknown3, unknown3, sizeof(pkt.unknown3));
  memcpy (pkt.unknown4, unknown4, sizeof(pkt.unknown4));
  pkt.unknown1 = 0xAAAA;
  pkt.control = 0x03;
  pkt.htype = 0x0202;
  for (i=0; i<sizeof(pkt.unknown5); i++) {
    pkt.unknown5[i] = 0x00;
  }
  for (i=0; i<sizeof(pkt.unknown6); i++) {
    pkt.unknown6[i] = 0x00;
  }
  printf ("Packet dump:");
  ptr = (uint8_t *) &pkt;
  for (i=0; i<sizeof(pkt); i++) {
    if (!(i % 16)) {
      printf("\n");
    } 
    printf ("%02x ", ptr[i]);
  }
  printf ("\n");

#ifdef __FreeBSD__
  if ((bytes = write(sd, &pkt, sizeof(pkt))) < 0) {
    perror("write to bpf device failed");
    exit(EXIT_FAILURE);
  }
#else
  sin.sin_addr.s_addr = inet_addr(target_ip);
  sin.sin_port = 0; /* not necessary in RAW */
  sin.sin_family = AF_INET; /* necessary for Linux */
 
  // Send ethernet frame to socket.
  if ((bytes = sendto (sd, (void*) &pkt, sizeof(pkt), 0, (struct sockaddr *) &sin, sizeof(sin))) <= 0) {
    perror ("sendto() failed");
    exit (EXIT_FAILURE);
  }
#endif
  printf ("sent %d byes successfully\n", bytes);
 
  // Close socket descriptor.
  close (sd);

  exit(EXIT_SUCCESS);
}
